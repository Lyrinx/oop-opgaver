﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ugeopgave
{
    class Menu : MenuItemBase
    {
        private bool _running;
        private int selectedIndex = 0;
        protected MenuItemContainer menuItemContainer;
        public void AddMenuItem(MenuItemBase menuItemToAdd)
        {
            menuItemContainer.Add(menuItemToAdd);
        }

        private void HandleInput()
        {
            MenuItemBase CurrentMenuItem = menuItemContainer.Get(selectedIndex);
            ConsoleKeyInfo cki = Console.ReadKey();
            switch (cki.Key)
            {
                case ConsoleKey.Backspace:
                case ConsoleKey.Escape:
                    _running = false;
                    break;
                case ConsoleKey.UpArrow:
                    MoveUp();
                    break;
                case ConsoleKey.DownArrow:
                    MoveDown();
                    break;
                case ConsoleKey.Enter:
                    CurrentMenuItem.Select();
                    break;
            }
        }

        private void DrawMenu()
        {
            Console.Clear();
            Console.ResetColor();
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.WriteLine(Title);
            Console.ResetColor();
            for (int index = 0; index < menuItemContainer.Count; index++)
            {
                if (index == selectedIndex)
                    Console.BackgroundColor = ConsoleColor.Green;
                Console.WriteLine(menuItemContainer.Get(index).Title);
                Console.ResetColor();
            }
        }

        private void MoveUp()
        {
            if(selectedIndex > 0)
                selectedIndex--;
        }

        private void MoveDown()
        {
            if (selectedIndex < (menuItemContainer.Count - 1))
                selectedIndex++;
        }


        public Menu(string Title) : base(Title)
        {
            menuItemContainer = new MenuItemContainer();
        }

        public Menu(string Title, params MenuItemBase[] menuItems) : base(Title)
        {
            menuItemContainer = new MenuItemContainer();
            foreach(MenuItemBase element in menuItems)
            {
                menuItemContainer.Add(element);
            }
        }

        public override void Select()
        {
            this.Start();
        }

        public void Start()
        {
            _running = true;
            do
            {
                DrawMenu();
                HandleInput();
            } while (_running);
        }
    }

    
}
