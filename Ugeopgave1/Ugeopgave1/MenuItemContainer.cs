﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ugeopgave
{
    class MenuItemContainer
    {
        public int Count { get; private set; }
        private MenuItemBase[] MenuItemArray;

        public MenuItemContainer()
        {
            Count = 0;
            MenuItemArray = new MenuItemBase[1];
        }

        public MenuItemBase Get(int index)
        {
            return MenuItemArray[index];
        }

        public void Add(MenuItemBase item)
        {
            if(MenuItemArray.Length == Count)
            {
                IncreaseArraySize(ref MenuItemArray);
            }
            Count++;
            MenuItemArray[MenuItemArray.Length - 1] = item;
        }

        private void IncreaseArraySize(ref MenuItemBase[] MenuItemArray)
        {
            Array.Resize(ref MenuItemArray, MenuItemArray.Length + 1);

            //MenuItem[] tempArray = new MenuItem[MenuItemArray.Length + 1];
            //Array.Copy(MenuItemArray, tempArray, MenuItemArray.Length);
            //MenuItemArray = tempArray;
        }
    }
}
