﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ugeopgave
{
    abstract class MenuItemBase
    {
        public MenuItemBase(string Title)
        {
            this.Title = Title;
        }
        public virtual string Title { get; }
        public abstract void Select();
    }
}
