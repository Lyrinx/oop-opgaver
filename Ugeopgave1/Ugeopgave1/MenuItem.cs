﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ugeopgave
{
    class MenuItem : MenuItemBase
    {
        public string Content { get; }

       

        public MenuItem(string Title, string Content) : base(Title){
            this.Content = Content;
        }
        public MenuItem(string Title) : this(Title, "")
        {
           
        }

        public override void Select()
        {
            Console.Clear();
            Console.ResetColor();
            Console.WriteLine(this.Title);
            Console.WriteLine(this.Content);
            Console.ReadKey();
        }
    }
}
