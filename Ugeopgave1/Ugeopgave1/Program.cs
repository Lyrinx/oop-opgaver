﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ugeopgave
{
    class Program
    {
        static void Main(string[] args)
        {
            //Menu menu = new Menu("FancyMenu");
            //MenuItem langtpunkt =
            //    new MenuItem(
            //        "Et lidt længere menupunkt",
            //        "Indhold af punkt 3... det er indtil videre også bare tekst"
            //        );
            //menu.AddMenuItem(langtpunkt);
            //menu.AddMenuItem(new MenuItem("Punkt1"));
            //menu.AddMenuItem(new MenuItem("Punkt2"));

            Menu menu = new Menu("FancyMenu");
            menu.AddMenuItem(new MenuItem("Punkt1"));
            menu.AddMenuItem(new MenuItem("Punkt2"));
            Menu underMenu = new Menu("undermenu",
                new MenuItem("testpunkt"),
                new MenuItem("testpunkt2"));
            menu.AddMenuItem(underMenu);
            InfiniteMenu infiniMenu = new InfiniteMenu("Infinimenu");
            menu.AddMenuItem(infiniMenu);
            menu.Start();
        }

    }
}
