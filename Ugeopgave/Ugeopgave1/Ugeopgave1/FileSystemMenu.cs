﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ugeopgave1
{
    class FileSystemMenu
    {

        protected DirectoryInfo DI { get; set; }
        public FileSystemMenu(DirectoryInfo directoryInfo, string title)
        {
            this.DI = directoryInfo;
        }

        protected FileSystemMenu(DirectoryInfo directoryInfo)
        {
            this.DI = directoryInfo;
        }
    }
}
