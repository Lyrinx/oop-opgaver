﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _15marts2016
{
    class Program
    {
        static void Main(string[] args)
        {

            List<int> numbers = new List<int>();
            Random r = new Random();
            int randomNum = 0;
            for (int i = 1; i < 100; i++)
            {
                randomNum = r.Next(0, 100); //random number between 0 and 100
                numbers.Add(randomNum);
            }
            // 1. Find tal der er multiplum af værdien af en ydre variabel(du definerer denne variabel)
            {
                int multipleToFind = 4;
                IEnumerable<int> listOfNumbers = numbers;
                listOfNumbers = listOfNumbers.Where(number => number % multipleToFind == 0);
                Console.WriteLine("Printing multiples of " + multipleToFind);
                foreach (int element in listOfNumbers)
                {
                    Console.WriteLine(element);
                }
                Console.WriteLine("Done");
                Console.ReadKey();
            }
            // 2. Find alle tal der er mellem MAX og MIN.MAX og MIN er to ydre variabler du definerer, f.eks. 20 og 40
            {
                int maxValue = 20;
                int minValue = 14;
                IEnumerable<int> listOfNumbers = numbers;
                listOfNumbers = listOfNumbers.Where(number => (number < maxValue) && (number > minValue));
                Console.WriteLine("Printing values between " + minValue + " and " + maxValue);
                foreach (int element in listOfNumbers)
                {
                    Console.WriteLine(element);
                }
                Console.WriteLine("Done");
                Console.ReadKey();
            }
            // 3. Find det største tal mellem MAX og MIN, f.eks. 38, hvis MAX=40 og MIN=20.
            {
                int maxValue = 80;
                int minValue = 15;
                IEnumerable<int> listOfNumbers = numbers;
                Console.WriteLine("The largest value in the list of numbers is: ");
                Console.WriteLine(listOfNumbers.Max<int>());
                Console.WriteLine("Done");
                Console.ReadKey();
            }
            // 4. Gange alle tallene med en ydre varabel.
            {
                int multipleValue = 3;
                Console.WriteLine("Printing all values of numbers, multiplied by " + multipleValue);
                foreach (int element in numbers)
                {
                    Console.WriteLine(element * multipleValue);
                }
                Console.WriteLine("Done");
                Console.ReadKey();
            }
            // 5. Sortér tallene i omvendt rækkefølge
            {
                IEnumerable<int> listOfNumbers = numbers;
                listOfNumbers = listOfNumbers.OrderByDescending(number => number);
                Console.WriteLine("Printing all values of numbers in numerical order");
                foreach (int element in listOfNumbers)
                {
                    Console.WriteLine(element);
                }
                Console.WriteLine("Done");
                Console.ReadKey();
            }
            //Kombinér 2, 4, og 5 i et enkelt udtryk.
            {
                //Find alle tal der er mellem MAX og MIN.MAX og MIN er to ydre variabler du definerer, f.eks. 20 og 40
                //Gange alle tallene med en ydre varabel.
                //Sortér tallene i omvendt rækkefølge
                
                int maxValue = 20;
                int minValue = 14;
                int multipleVariable = 3;
                IEnumerable<int> listOfNumbers = numbers;
                listOfNumbers = listOfNumbers.Where(number => (number < maxValue) && (number > minValue)).Select(number => number * 3).OrderByDescending(number => number);
                Console.WriteLine("Sorting between min and max, multiplying by 3 and then sorting");
                foreach (int element in listOfNumbers)
                {
                    Console.WriteLine(element);
                }
                Console.WriteLine("Done");
                Console.ReadKey();
            }

            List<Person> people = new List<Person>()
            {
                new Person() { Name = "Ib", Weight = 89.6, Age = 27 },
                new Person() { Name = "Kaj", Weight = 65.7, Age = 17 },
                new Person() { Name = "Ole", Weight = 77, Age = 7 },
                new Person() { Name = "Anders", Weight = 72, Age = 40 },
                new Person() { Name = "Børge", Weight = 88.8, Age = 13 }
            };
            
            //Sortér personerne iflg.deres vægt
            IEnumerable<Person> sortedByWeight = people.OrderBy(person => person.Weight);
            //Sortér på navn i omvendt rækkefølge
            IEnumerable<Person> sortedByDescName = people.OrderByDescending(person => person.Name);
            //Returnér navnene, og KUN navnene (string), på alle personer der har et a, stort eller lille, i deres navn.
            IEnumerable<Person> containsLetterA = people.Where(person => person.Name.ToLower().Contains("a"));
            //Find vægten på den teenager der vejer mest
            IEnumerable<Person> weightyTeenager = people.Where(person => person.Age < 20).Max(person => person.Weight);
        }
        public class Person
        {
            public string Name { get; set; }
            public double Weight { get; set; }
            public int Age { get; set; }
        }
    }
}
