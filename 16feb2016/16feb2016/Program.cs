﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _16feb2016
{
    class Person
    {
        public string Fornavn;
        public string Efternavn;
        public int Alder;
        public Person Father;
        public Person Mother;

        public int PersonID
        {
            get { return PersonID; }
            set { PersonID = GenPersonID(); }
        }

        private static int numberOfIDs = 0;
        private static int GenPersonID()
        {
            return ++numberOfIDs;
        }

        public Person(string Fornavn, string Efternavn, int Alder, Person Father, Person Mother)
        {
            this.Fornavn = Fornavn;
            this.Efternavn = Efternavn;
            this.Alder = Alder;
            this.Father = Father;
            this.Mother = Mother;
        }

        public Person(string Fornavn, string Efternavn, int Alder)
        {
            this.Fornavn = Fornavn;
            this.Efternavn = Efternavn;
            this.Alder = Alder;
        }

        static void swapPersons(Person p1, Person p2){
            Person tmp = p1;
            p1 = p2;
            p2 = tmp;
        }

        static void printPerson(Person p1)
        {
            Console.WriteLine("Name: " + p1.Fornavn + p1.Efternavn);
            Console.WriteLine("Age: " + p1.Alder);
            Console.WriteLine("Father: " + p1.Father.Fornavn);
            Console.WriteLine("Mother: " + p1.Mother.Fornavn);
        }
    }


    class Vector3
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }

        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Vector3 Add(Vector3 vector1, Vector3 vector2)
        {
            return new Vector3(vector1.x + vector2.x, vector1.y + vector2.y, vector1.z + vector2.z);
        }

        public Vector3 Subtract(Vector3 vector1, Vector3 vector2)
        {
            return new Vector3(vector1.x - vector2.x, vector1.y - vector2.y, vector1.z - vector2.z);
        }

        public Vector3 Multiply(Vector3 inputVector, float scalar)
        {
            return new Vector3(inputVector.x * scalar, inputVector.y * scalar, inputVector.z * scalar);
        }

        public void ImMultiply(Vector3 inputVector, float scalar)
        {
            inputVector.x = inputVector.x * scalar;
            inputVector.y = inputVector.y * scalar;
            inputVector.z = inputVector.z * scalar;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            //DirectoryInfo di = new DirectoryInfo("C:\\opencv\\");
            //DirectoryInfo[] myDirectories = di.GetDirectories("*", SearchOption.TopDirectoryOnly);
            //FileInfo[] myFiles = di.GetFiles("*", SearchOption.TopDirectoryOnly);


            //foreach(FileInfo element in myFiles)
            //{
            //    Console.WriteLine(element.Name);
            //    Console.WriteLine(element.Length);
            //}

            //Console.ReadKey();

            //foreach (DirectoryInfo element in myDirectories)
            //{
            //    Console.WriteLine(element.Name);
            //    Console.WriteLine(element.GetDirectories("*", SearchOption.AllDirectories).Length + "folders");
            //    Console.WriteLine(element.GetFiles("*", SearchOption.AllDirectories).Length + "files total");
            //    Console.ReadKey();
            //}

            //Person p1 = new Person("first1", "last1", 15);
            //Person p2 = new Person("first2", "last2", 19);

            //Console.WriteLine(p1 == p2);
            //p1 = p2;
            //Console.WriteLine(p1 == p2);
            //Console.WriteLine(p1);
            //Console.WriteLine(p2);

            Console.ReadKey();
        }
    }
}
