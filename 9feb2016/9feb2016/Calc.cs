﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9feb2016
{
    class Calc
    {
        private double _tal = 0;

        public double Plus(double _tal)
        {
            double input = 0;
            Console.WriteLine(string.Format("{0} plus what?", _tal));
            try
            {
                input = double.Parse(Console.ReadLine());
            }
            catch(FormatException e)
            {
                Console.WriteLine(e);
            }
            return _tal = _tal + input;
        }
        public double Minus(double _tal)
        {
            double input = 0;
            Console.WriteLine(string.Format("{0} minus what?", _tal));
            try
            {
                input = double.Parse(Console.ReadLine());
            }
            catch (FormatException e)
            {
                Console.WriteLine(e);
            }
            return _tal = _tal - input;
        }
        public double Times(double _tal)
        {
            double input = 0;
            Console.WriteLine(string.Format("{0} times what?", _tal));
            try
            {
                input = double.Parse(Console.ReadLine());
            }
            catch (FormatException e)
            {
                Console.WriteLine(e);
            }
            return _tal = _tal * input;
        }
        public double Divide(double _tal)
        {
            double input = 0;
            Console.WriteLine(string.Format("{0} divided with what?", _tal));
            try
            {
                bool isNonZero = true;
                while(isNonZero){
                    input = double.Parse(Console.ReadLine());
                    if (input == 0) {
                        isNonZero = false;
                        Console.WriteLine("Input is zero, try again");
                    }
                }
            }
            catch (FormatException e)
            {
                Console.WriteLine(e);
            }
            return _tal = _tal / input;
        }

        public void Calculate()
        {
            Console.WriteLine("What is your first input?");
            double input = double.Parse(Console.ReadLine());
            Console.WriteLine("Which operator? (+, -, /, *");
            char charInput;
            bool isDone = false;
            while (!isDone)
            {
                charInput = char.Parse(Console.ReadLine());
                switch (charInput)
                {
                    case '+':
                        {
                            Console.WriteLine(Plus(input));
                            break;
                        }
                    case '-':
                        {
                            Console.WriteLine(Minus(input));
                            break;
                        }
                    case '/':
                        {
                            Console.WriteLine(Divide(input));
                            break;
                        }
                    case '*':
                        {
                            Console.WriteLine(Times(input));
                            break;
                        }
                    case '=':
                    case '\n':
                        {
                            isDone = true;
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Input parameter not recognized");
                            break;
                        }
                }
                
            }
        }


    }
}
