﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9feb2016
{
    class Person
    {
        public string Fornavn;
        public string Efternavn;
        public int Alder;
        public Person Father;
        public Person Mother;


        static void swapPersons(Person p1, Person p2){
            Person tmp = p1;
            p1 = p2;
            p2 = tmp;
        }

        static void printPerson(Person p1)
        {
            Console.WriteLine("Name: " + p1.Fornavn + p1.Efternavn);
            Console.WriteLine("Age: " + p1.Alder);
            Console.WriteLine("Father: " + p1.Father.Fornavn);
            Console.WriteLine("Mother: " + p1.Mother.Fornavn);
        }
    }

    class Program
    {
        static double Sum(double x1, double x2)
        {
            return x1 + x2;
        }
        static double Sum(double x1, double x2, double x3)
        {
            return x1 + x2 + x3;
        }
        static double Sum(params double[] numbers)
        {
            double sum = 0;
            foreach(double number in numbers){
                sum += number;
            }
            return sum;
        }
        
        static void Swap(ref int x, ref int y){
            y = x ^ y;
            x = x ^ y;
            y = x ^ y;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Opgave 1,a");
            Console.WriteLine("-------------------------");
            Console.WriteLine("Please enter a number: ");
            int input1;

            while (!int.TryParse(Console.ReadLine(), out input1))
            {
                Console.WriteLine("Input is not a number, try again");
                Console.ReadLine();
            }
            Console.WriteLine(input1);
            Console.WriteLine("-------------------------");

            Console.WriteLine("Opgave 1,b");
            Console.WriteLine("-------------------------");
            Console.WriteLine("Please enter a number: ");
            bool isSuccessful = false;
            int input2 = 0;
            while (!isSuccessful)
            {
                try
                {
                    input2 = int.Parse(Console.ReadLine());
                    isSuccessful = true;
                }
                catch (ArgumentNullException e)
                {
                    isSuccessful = false;
                    Console.WriteLine(e);
                    Console.WriteLine("Input not read correctly, please try again");
                }
                catch (FormatException e)
                {
                    isSuccessful = false;
                    Console.WriteLine(e);
                    Console.WriteLine("Input not a number, please try again");
                }
                catch (OverflowException e)
                {
                    isSuccessful = false;
                    Console.WriteLine(e);
                    Console.WriteLine("Number is too large, please try again");
                }
            }

            Console.WriteLine(string.Format("Written input is: {0}", input2));
            Console.WriteLine("-------------------------");
            Console.WriteLine("Opgave 1,c");
            Console.WriteLine("As hexadecimal");
            Console.WriteLine(input2.ToString("X"));
            Console.WriteLine("-------------------------");
            Console.WriteLine("Opgave 2");
            Console.WriteLine(Sum(1, 2, 3));
            Console.WriteLine(Sum(1, 2, 3, 4));
            Console.WriteLine("-------------------------");
            Console.WriteLine("Opgave 3");
            //Calc myCalc = new Calc();
            //myCalc.Calculate();
            Console.ReadKey();
            Console.WriteLine("Opgave 4");
            Console.WriteLine("Enter input 1 to swap");
            int input3 = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter input 2 to swap");
            int input4 = int.Parse(Console.ReadLine());
            Console.WriteLine(string.Format("Before swap a: {0}, b: {1}", input3, input4));
            Swap(ref input3, ref input4);
            Console.WriteLine(string.Format("After swap a: {0}, b: {1}", input3, input4));

            Person Far = new Person();
            Person Mor = new Person();
            Mor.Alder = 400;
            Mor.Efternavn = "Efternavn";
            Mor.Fornavn = "Moder";
            Far.Alder = 1144123;
            Far.Efternavn = "Efternavn";
            Far.Fornavn = "Fader";
            Person Jeg = new Person();
            Jeg.Alder = 1232;
            Jeg.Efternavn = "Efternavn";
            Jeg.Fornavn = "Jeg";
            Jeg.Father = Far;
            Jeg.Mother = Mor;
            Console.ReadKey();
        }
    }
}
