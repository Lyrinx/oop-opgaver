﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8marts2016
{
    public class MySet<T>
    {
        public List<T> mySet { get; }

        public MySet()
        {
            mySet = new List<T>();
        }
        public void Add(T addable)
        {
            if (mySet.Contains(addable)){
                throw new ValueNotUniqueException();
            }
            else
            {
                mySet.Add(addable);
            }
        }
        public void Remove(T removeable)
        {
            mySet.Remove(removeable);
        }

        public List<T> toList(List<T> list)
        {
            return list;
        }
    }

    class ValueNotUniqueException : System.Exception
    {
        public ValueNotUniqueException() { }
        public ValueNotUniqueException(string message) : base(message) { }
        public ValueNotUniqueException(string message, Exception innerException) : base(message, innerException) { }
    }
}
