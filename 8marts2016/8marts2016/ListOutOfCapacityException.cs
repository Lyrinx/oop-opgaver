﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8marts2016
{
    class ListOutOfCapacityException : System.Exception
    {
        public ListOutOfCapacityException() { }
        public ListOutOfCapacityException(string message) : base(message) { }
        public ListOutOfCapacityException(string message, Exception innerException) : base(message, innerException) { }

    }
}
