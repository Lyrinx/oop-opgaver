﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8marts2016
{
    class MyQueue<T>
    {
        public List<T> Queue { get; }
        private int maxSize { get; set; }
        public T Head { get; }

        MyQueue()
        {
            Queue = new List<T>();
            Head = Queue[0];
            maxSize = 0;
        }

        MyQueue(int capacity)
        {
            Queue = new List<T>();
            Head = Queue[0];
            maxSize = capacity;
        }
        public void Enqueue(T obj)
        {
            if(maxSize > Queue.Count || maxSize == 0)
                Queue.Add(obj);
            else
            {
                throw new ListOutOfCapacityException();
            }
        }

        public T Dequeue()
        {
            T obj = Queue[0];
            Queue.RemoveAt(0);
            return obj;
        }
    }
}
