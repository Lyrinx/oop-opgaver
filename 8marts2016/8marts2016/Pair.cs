﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8marts2016
{
    class Pair<T>
    {
        public T First { get; }
        public T Second { get; }

        Pair(T first, T second)
        {
            First = first;
            Second = second;
        }

        void Swap(ref T a, ref T b)
        {
            T tmp = a;
            a = b;
            b = tmp;
        }

    }
}
