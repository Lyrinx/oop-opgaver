﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8marts2016
{
    class HandyMethods
    {
        public static T Min<T>(params T[] list) where T : IComparable
        {
            T min = default(T);
            if (list.Length != 0)
            {
                foreach (T element in list)
                {
                    if (element.CompareTo(min) < 0)
                    {
                        min = element;
                    }
                }
            }
            return min;
        }
        public static T Max<T>(params T[] list) where T : IComparable
        {
            T max = default(T);
            if (list.Length != 0)
            {
                foreach (T element in list)
                {
                    if (element.CompareTo(max) > 0)
                    {
                        max = element;
                    }
                }
            }
            return max;
        }

    }
}
